def call(body)
{
    withCredentials([string(credentialsId: 'ANSADMIN_PASSWORD', variable: 'ansadmin_password')]){
        sh 'sshpass -p ${ansadmin_password} scp -v sb.yml ansadmin@172.31.22.13:/opt/playbooks'
        sh 'sshpass -p ${ansadmin_password} ssh -v -o StrictHostKeyChecking=no ansadmin@172.31.22.13 \"cd /opt/playbooks\target; wget -O sb.war http://18.224.155.110:8081/nexus/content/repositories/sb/sb-${BUILD_NUMBER}.war\"'
        sh 'sshpass -p ${ansadmin_password} ssh -v -o StrictHostKeyChecking=no ansadmin@172.31.22.13 \"cd /opt/playbooks; ansible-playbook /opt/playbooks/sb.yml\"'
    }
}
